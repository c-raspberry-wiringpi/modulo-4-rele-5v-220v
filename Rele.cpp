#include <stdio.h>
#include <wiringPi.h>
//Codigo para utilizar un modulo rele de 5v-220v


int main(int argc, char** argv)
{
	wiringPiSetup(); //Iniciar la libreria WiringPi
		pinMode(29, OUTPUT); // Se declara el pin29 como salida
		digitalWrite(29, LOW); //Encendido del Rele 
		delay(3000); //delay de 3 segundos 
		digitalWrite(29, HIGH); //Apagado del rele
		delay(3000); // delay de 3 segundos
return 0; //Final del programa
}